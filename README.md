Goldenzone
==========
## Description
This is a blueprint för a typical JEE project that is to be handled in a CD pipeline.

It is divided into the following sub-modules:
- common, this is common libraries used by all modules
- appl, this is the actual application with services, information model, user interface, etc.
- test, this is the various test modules: integration test, BDD tests, performance tests, test tools, etc.
- tools, this is various tools to be used in various activities such as development, test, installation, etc.
- etc, this is non-buildable things, such as jenkins jobs, shell scripts, configuration data, etc.

## Build
The main artifacts is built with:

    $ mvn clean install

## Run integration tests
To build and run integration tests

    $ mvn clean install -Pint-test


