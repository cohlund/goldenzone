package com.caglabs.goldenzone.managerui.ui;

import com.caglabs.goldenzone.common.BuildInfo;
import com.caglabs.goldenzone.helloservice.client.Hello;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;


/**
 * The Application's "main" class
 */
public class ManagerUI extends UI {
    private static Hello helloService;

    @Override
    protected void init(VaadinRequest request) {
        final VerticalLayout layout = new VerticalLayout();
        layout.setMargin(true);
        setContent(layout);
        layout.setSpacing(true);
        layout.setMargin(true);

        final TextField nameTextFiled = new TextField("Name");
        layout.addComponent(nameTextFiled);

        Button helloButton = new Button("Say hello via the Hello service");
        helloButton.addClickListener(new Button.ClickListener() {
            public void buttonClick(ClickEvent event) {
                Hello hello = helloService;
                try {
                    layout.addComponent(new Label(hello.sayHello(nameTextFiled.getValue())));
                } catch (Hello.HelloException e) {
                    layout.addComponent(new Label(e.getLocalizedMessage()));
                }
            }
        });
        helloButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        helloButton.addStyleName("primary");
        layout.addComponent(helloButton);
        layout.addComponent(new Label(""));
        layout.addComponent(new Label("Application version: " + BuildInfo.getApplicationVersion()));
        layout.addComponent(new Label("Build tag: " + BuildInfo.getBuildTag()));
        layout.addComponent(new Label("Revision: " + BuildInfo.getRevision()));
        layout.addComponent(new Label("Build no: " + BuildInfo.getBuildNumber()));
    }

    @WebServlet(
            urlPatterns = "/*",
            initParams = {
                    @WebInitParam(name = "UI", value = "com.caglabs.goldenzone.managerui.ui.ManagerUI"),
                    @WebInitParam(name = "widgetset", value = "com.caglabs.goldenzone.managerui.ui.AppWidgetSet")
            })
    public static class ManagerUiVaadinServlet extends com.vaadin.server.VaadinServlet {
        @EJB
        private Hello hello;

        @PostConstruct
        public void init() {
            System.out.println("Initialized hello=" + hello);
            // Fudge to inject Hello service into UI. This is not the right way to do it, but it will suffice for now.
            helloService = hello;
        }
    }
}
