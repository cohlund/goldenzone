package com.caglabs.goldenzone.infomodel.entity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GoldenzoneUserDaoTest extends AbstractEntityTestBase {
    private GoldenzoneUserDao goldenzoneUserDao;

    @Before
    public void setup() {
        goldenzoneUserDao = new GoldenzoneUserDao();
        injectEntityManager(goldenzoneUserDao);
    }

    @Test
    public void testGetUserByName() {
        withinTransaction(new Runnable(){
            @Override
            public void run() {
                goldenzoneUserDao.save(new GoldenzoneUser("bob", "Bobby Banan"));
            }
        });
        GoldenzoneUser bob = goldenzoneUserDao.getSystemUserByName("bob");
        assertEquals("Bobby Banan", bob.getDisplayName());
    }
}
