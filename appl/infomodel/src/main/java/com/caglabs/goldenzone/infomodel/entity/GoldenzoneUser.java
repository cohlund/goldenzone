package com.caglabs.goldenzone.infomodel.entity;

import javax.persistence.*;

@Entity
@NamedQueries(value = {
        @NamedQuery(name = "GetSystemUserByName", query = "SELECT su FROM GoldenzoneUser su WHERE su.name = :name")
})
@Table(name = "goldenzone_user",
        uniqueConstraints = @UniqueConstraint(columnNames = {"name"})
)
public class GoldenzoneUser {
    public static final String GET_BY_NAME = "GetSystemUserByName";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Version
    private Integer version;


    @Column(name = "name")
    private String name;

    @Column(name = "display_name")
    private String displayName;

    public GoldenzoneUser(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    public GoldenzoneUser() {
    }

    public Long getId() {
        return this.id;
    }

    public Integer getVersion() {
        return this.version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("GoldenzoneUser");
        sb.append("{id=").append(id);
        sb.append(", version=").append(version);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

